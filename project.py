#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#######################################
      LSPyIV -- PROJECT MODULE
#######################################
Python LSPIV tools

Defines the lspivProject class. Contains all the functions used
to extract and diplay datas from an existing Fudaa-LSPIV project. 

"""

import numpy as np
import os 
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from zipfile import ZipFile
from lspiv import velocities,transform
from tqdm import tqdm 
from tkinter.filedialog import askdirectory
import glob
from PIL import Image
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
import scipy.stats

class lspivProject():
     """
     Fudaa-LSPIV project class:
     
    Contains 
    ---------------------
        - grid [npArrayInt]
            Grid points indexes -> stored [[index],[i,j]] 
        
        - h [float]
            Water level [m]
        
        - list_avg [npArrayStr]
            List of velocity fields used to compute mean velocity field
            
        - stab_param [dict]
            Dictionary containing information about stabilisation module
            
            - 'enable' [bool]
            
            - 'keypointDens' [str]
                Density of keypoints used (similar to "quality")
            
            - 'transfModel' [str]
                Transformation model used 
            
        - GRP [grp class]
            Class containing information about Ground Reference Points
            
            - X [float]
                X position in world [m]
            - Y [float]
                Y position in world [m]
            - Z [float]
                Z position in world [m]
            - i [int]
                i index on image [pixel]
            - j [float]
                j index on image [pixel]
        
        - img_ref [imgRef class]
            Class containing image references:  
                
            - xyMin [tuple]
                Minimum values of X and Y used for ortho-rectification -> stored (X,Y)
            
            - xyMax [tuple]
                Maximum values of X and Y used for ortho-rectification -> stored (X,Y)
            
            - res [float]
                Ortho-rectification resolution [m/pix]  
            
            - ninj [tuple]
                Size of the ortho-rectified image [pix] -> stored (ni,nj)
                
        - PIV_param [pivParam class]
            Class containing PIV parameters:
                
            - SA [dict]
                Dictionary containing Searching Area informations 
                ( S + i,j (in image order) + m,p ("minus", "plus") )
                
                ------'Sim'------
                \       \        \
               'Sjm'    xx      'Sjp'
                 \       \        \
                  ------'Sip'------   
                                    
            - IA [int]
                 Size of Interrogation Area (squarred area of side IA) 
                    
            - dt [float]
                Time interval [sec]
                
            - rMaxMin [tupleFloat]
                Filters on correlation -> stored (rMin,rMax)
                
            - nMaxMin [tupleFloat]
                Filters on norm of velocity [m/s] -> stored (nMin,nMax)
                    
            - vxMaxMin [tupleFloat]
                Filters on X velocity [m/s] -> stored (vxMin,vxMax)
                    
            - vyMaxMin [tuple]
                Filters on Y velocity [m/s] -> stored (vyMin,vyMax)
                    
            - alphaCoeff [float]
                Surface coefficient used to determine height average velocity  
                    
            - rxyMax [tupleFloat]
                Averaging radius around bathy point
                    
            - Dxp [float]
                Interpolation step for bathy points 
    ---------------------------                      
    
    """
    
     def __init__(self,name): 
        self.name       = name  
        self.grid       = np.empty(1, dtype = int)
        self.h          = -99.99
        self.list_avg   = np.empty(1, dtype = str)
        self.stab_param = {
                          'enable'      :False,
                          'keypointDens':"average",
                          'tranfModel'  :"Projective"
                            }
        self.GRP        = []                    # list of class GRP()
        self.img_ref    = transform.imgRef()    # Image parameters 
        self.PIV_param  = velocities.pivParam() # PIV parameters
        self.vel_raw    = []                    # list of class vel()
        self.vel_filtre = []                    # list of class vel()
        self.vel_filtre_pp  = []
        self.vel_raw_pp     = []
        self.vel_moy    = velocities.vel()      # list of average velocities 
        self.contains   = {}                    # Dictionnary resuming files in the project 
        self.img_src    = []                    # List of source images 
        self.img_transf = []                    # List of transformed images 
        self.mask       = []                    # Mask of river. Needed for several routines
        self.transect   = []                    # List of bathy() 
        self.Q          = {}
        self.vel_ens    = []                    # Ensemble velocity 
        self.rGrid      = []                    # correlation field (ensemble)
     
     def readReport(self,filename = ""):
        """
         Read data from Fudaa-LSPIV Report (.xlsx). 

         Parameters
         ----------
            filename [str] 
                - Path to the Fudaa-LSPIV project. 
                  If filename = "" then a GUI will be open to find the file  
                  
        Returns
        ----------------------
        
            errno [int]
                - 0 - No errors
                - 1 - Format isn't ".lspiv.zip" 
                - 2 - File "img_ref.dat" is missing
                - 3 - File "PIV_param.dat is missing

         """
         # ---- Initialisation 
        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename(title = "Open Fudaa-LSPIV Report", filetypes = (("lspiv report","*.xlsx"),("all files","*.*"))) # Get file path
        
        if (filename[-5:] != ".xlsx" ):
            print("ERROR : file format doesn't match\n")
            return 1
         
        # ---- Read file 
        df = pd.read_excel(filename,header = None)
        
        # Name 
        if df.iat[6,5] == "-":
            self.name = filename.split("/")[-1][:-5]
        else:
            self.name   = df.iat[6,5]
        # h
        h           = df.iat[4,5]
        self.h      = float(h[:-1])
        # Q    
        q           = df.iat[4,15]
        self.q      = float(q[:-4])        
        q_r         = df.iat[5,15]
        self.q_r    = float(q_r[:-1])
        # Array with results / transect
        # self.q_mes  = np.zeros(int(df.iat[30,6]))
        # for k in range(int(df.iat[30,6])):
        #     self.q_mes[k] = float(df.iat[62+k,15])
        # # U 
        # self.u_m    = np.zeros(int(df.iat[30,6]))
        # for k in range(int(df.iat[30,6])):
        #     self.u_m[k] = float(df.iat[62+k,11])
        # # Qtot
        # self.q_tot  = np.zeros(int(df.iat[30,6]))
        # for k in range(int(df.iat[30,6])):
        #     self.q_tot[k] = float(df.iat[62+k,3])
        # nb im 
        nb_img      = df.iat[11,6]
        self.nb_img = int(nb_img)
        # im ref
        self.img_ref.xyMin  = (float(df.iat[14,11][:-1]),float(df.iat[15,11][:-1]))
        self.img_ref.xyMax  = (float(df.iat[14,15][:-1]),float(df.iat[15,15][:-1]))
        # res 
        if isinstance(df.iat[16,5],str):
            self.img_ref.res    = float(df.iat[16,5][:-5])
        elif isinstance(df.iat[16,6],str):
            self.img_ref.res    = float(df.iat[16,6][:-5])
        # piv param 
        self.PIV_param.SA         ={
                                     'Sim' : int(df.iat[20,7][:-3]),
                                     'Sip' : int(df.iat[20,13][:-3]),
                                     'Sjm' : int(df.iat[21,7][:-3]),
                                     'Sjp' : int(df.iat[21,13][:-3])
                                     } 
        self.PIV_param.IA         = int(df.iat[19,7][:-3])
        self.PIV_param.dt         = float(df.iat[11,16][:-1])
        # R min-max filter
        if df.iat[27,11] != "-":
            rMin = float(df.iat[27,11])
        else:
            rMin = np.nan
        if df.iat[27,15] != "-":
            rMax = float(df.iat[27,15])
        else:
            rMax = np.nan
        self.PIV_param.rMaxMin    =(rMin,rMax)
        self.PIV_param.ninj       =(-99   ,-99)
        
        # N min-max filter
        if df.iat[24,11] != "-":
            nMin = float(df.iat[24,11][:-3])
        else:
            nMin = np.nan
        if df.iat[24,15] != "-":
            nMax = float(df.iat[24,15][:-3])
        else:
            nMax = np.nan
        self.PIV_param.nMaxMin    =(nMin,nMax)# norm filter
        #Vx filter
        if df.iat[25,11] != "-":
            vxMin = float(df.iat[25,11][:-3])
        else:
            vxMin = np.nan
        if df.iat[25,15] != "-":
            vxMax = float(df.iat[25,15][:-3])
        else:
            vxMax = np.nan
        self.PIV_param.vxMaxMin   =(vxMin,vxMax)
        # Vy filter
        if df.iat[26,11] != "-":
            vyMin = float(df.iat[26,11][:-3])
        else:
            vyMin = np.nan
        if df.iat[26,15] != "-":
            vyMax = float(df.iat[26,15][:-3])
        else:
            vyMax = np.nan
        
        self.PIV_param.vyMaxMin   =(vyMin,vyMax)
        
        
        self.PIV_param.alphaCoeff = float(df.iat[33,11]) 
        
        self.PIV_param.rxyMax     =(float(df.iat[31,11][:-1]),float(df.iat[31,15][:-1]))
        self.PIV_param.dxp        =(float(df.iat[32,11][:-1]),float(df.iat[32,15][:-1]))

        self.notes                = df.iat[96,1] 
        
     def readfile(self,filename = ""):
        """ Read data from a Fudaa-LSPIV project (.lspiv.zip) 
        
        Parameters
        ----------------------
         
            filename [str] 
                - Path to the Fudaa-LSPIV project. 
                  If filename = "" then a GUI will be open to find the file  
                  
        Returns
        ----------------------
        
            errno [int]
                - 0 - No errors
                - 1 - Format isn't ".lspiv.zip" 
                - 2 - File "img_ref.dat" is missing
                - 3 - File "PIV_param.dat is missing
                
        Notes
        -----------------------
        Files could be missing such as grid.dat, h.dat, average_vel.out... 
        Still extraction will be performed and a warning message will be raised.
        To check the files that the lspivProject contains look at the .contains dictionnary
        
        """
        # ---- Initialisation 
        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename(title = "Open Fudaa-LSPIV Project", filetypes = (("lspiv project","*.lspiv.zip"),("all files","*.*"))) # Get file path
        
        if (filename[-10:] != ".lspiv.zip" ):
            print("ERROR : file format doesn't match\n")
            return 1
                
        # ---- Read ZIP
        # Open project using ZipFile 
        with ZipFile(filename,'r') as foldzip:
            
            # ---- -- Checkings 
            dictFiles = { 
                    'img_ref'       :foldzip.namelist().count("outputs.dir/img_ref.dat"),
                    'PIV_param'     :foldzip.namelist().count("outputs.dir/PIV_param.dat"),
                    'h'             :foldzip.namelist().count("outputs.dir/h.dat"),
                    'list_avg'      :foldzip.namelist().count("outputs.dir/list_avg.dat"),
                    'grid'          :foldzip.namelist().count("outputs.dir/grid.dat"),
                    'average_vel'   :foldzip.namelist().count("outputs.dir/average_vel.out"),
                    'average_scal'  :foldzip.namelist().count("outputs.dir/average_scal.out"),
                    'GRP'           :foldzip.namelist().count("outputs.dir/GRP.dat")
                    }
            
            # ---- -- img ref
            if dictFiles['img_ref']:
                data = foldzip.read("outputs.dir/img_ref.dat").decode("utf-8") 
                data = data.split("\n")
                # xy min
                tmp = data[1]
                tmp = tmp.split()   
                self.img_ref.xyMin  = (float(tmp[0]),float(tmp[1])) 
                    # xy max    
                tmp = data[3]
                tmp = tmp.split()
                self.img_ref.xyMax  = (float(tmp[0]),float(tmp[1])) 
                    # res
                tmp = data[5]
                self.img_ref.res    = float(tmp)
                    # ninj
                tmp = data[7]
                tmp = tmp.split()
                self.img_ref.ninj   = (int(tmp[0]),int(tmp[1]))   
            else:
                print("ERROR : file img_ref.dat missing, impossible to go further.\n")
                return 2
            
             # ---- -- PIV param 
            if dictFiles['PIV_param']:
                data = foldzip.read("outputs.dir/PIV_param.dat").decode("utf-8") 
                data = data.split("\n")
                    # IA
                tmp = data[1] 
                self.PIV_param.IA           = int(tmp) 
                    # SA
                self.PIV_param.SA['Sim']    = int(data[3])   
                self.PIV_param.SA['Sip']    = int(data[4])
                self.PIV_param.SA['Sjm']    = int(data[5])
                self.PIV_param.SA['Sjp']    = int(data[6])
                    # dt
                self.PIV_param.dt           = float(data[8])
                    # r
                self.PIV_param.rMaxMin      = (float(data[10]),float(data[12]))
                    # image size     
                self.PIV_param.ninj         = (int(data[15]),int(data[14]))
                    # s
                self.PIV_param.nMaxMin      = (float(data[17]),float(data[18]))
                    # Vx
                self.PIV_param.vxMaxMin     = (float(data[20]),float(data[21]))
                    # Vy 
                self.PIV_param.vyMaxMin     = (float(data[23]),float(data[24]))
                    # Coeff Surf
                self.PIV_param.alphaCoeff   = float(data[26])
                    # rxymax
                tmp = data[28]
                tmp = tmp.split()
                self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                    # Dxp
                self.PIV_param.dxp          = float(data[30])
            else:
                print("ERROR : file PIV_param.dat missing, impossible to go further.\n")
                return 3
                
            # ---- -- GRP.dat 
            if dictFiles['GRP']:
                buff = foldzip.read("outputs.dir/GRP.dat").decode("utf-8")
                data = buff.split("\n")
                nbGRP = int(data[1])
                data = data[3:]
                for ind in range(nbGRP):
                    elGRP = transform.GRP()
                    tmp = data[ind].split()
                    elGRP.X = float(tmp[0])
                    elGRP.Y = float(tmp[1])
                    elGRP.Z = float(tmp[2])
                    elGRP.i = int(tmp[3])
                    elGRP.j = int(tmp[4])
                    self.GRP.append(elGRP)
                #data = np.reshape(data,(-1,4))
                
            # ---- -- h.dat 
            if dictFiles['h']:
                data = foldzip.read("outputs.dir/h.dat").decode("utf-8") 
                self.h = float(data)
            else:
                print("WARNING : file h.dat is missing.\n")
            
            # ---- -- list avg
            if dictFiles['list_avg']:
                buff = foldzip.read("outputs.dir/list_avg.dat").decode("utf-8") 
                data = buff.split()
                self.list_avg = np.asarray(data,dtype = str)
            else:
                print("WARNING : file list_avg.dat is missing.\n")
            
            # ---- -- grid.dat 
            if dictFiles['grid']:
                    # Data extraction 
                buff = foldzip.read("outputs.dir/grid.dat").decode("utf-8")    
                data = buff.split()
                   # Create an array with datas
                data = np.asarray(data,dtype = int)
                    # Reshape array to get 2D array
                data = np.reshape(data,(-1,2))
                    # Reshape data 
                grid = np.array([data[:,1],data[:,0]]).T # Swap columns to get (i,j)
                grid[:,0] = self.PIV_param.ninj[0] - grid[:,0] # Inverse i axes to get proper image coordinate
                self.grid = grid 
            else:
                print("WARNING : file grid.dat is missing.\n")
            
            # ---- -- vel_moy 
            if dictFiles['average_vel']:
                    # average_vel.out
                buff = foldzip.read("outputs.dir/average_vel.out") 
                tmp = buff.split()
                data = np.reshape(np.asarray(tmp,dtype = float),(-1,4))
                self.vel_moy.x  = data[:,0].copy()
                self.vel_moy.y  = data[:,1].copy()
                self.vel_moy.vx = data[:,2].copy()
                self.vel_moy.vy = data[:,3].copy()
            else:
                print("WARNING : file average_vel.out is missing.\n")
            if dictFiles['average_scal']:
                    # average_scal.out
                buff = foldzip.read("outputs.dir/average_scal.out") 
                tmp = buff.split()
                data = np.reshape(np.asarray(tmp,dtype = float),(-1,6))
                self.vel_moy.n      = data[:,2].copy()
                self.vel_moy.r      = data[:,3].copy()
                    # Compute other quantities 
                self.vel_moy.theta  = np.arctan2(self.vel_moy.vy,self.vel_moy.vx)
                self.vel_moy.dx     = self.vel_moy.vx * self.PIV_param.dt
                self.vel_moy.dy     = self.vel_moy.vy * self.PIV_param.dt
                self.vel_moy.di     = -self.vel_moy.dy / self.img_ref.res
                self.vel_moy.dj     = self.vel_moy.dx / self.img_ref.res 
                self.vel_moy.vi    = self.vel_moy.di / self.PIV_param.dt 
                self.vel_moy.vj    = self.vel_moy.dj / self.PIV_param.dt
                self.vel_moy.i      = self.PIV_param.ninj[0] - ((self.vel_moy.y - self.img_ref.xyMin[1]) / self.img_ref.res)
                self.vel_moy.j      = (self.vel_moy.x - self.img_ref.xyMin[0]) / self.img_ref.res
                self.vel_moy.nij    = np.sqrt(self.vel_moy.di**2 + self.vel_moy.dj**2)
                self.vel_moy.theij  = np.arctan2(self.vel_moy.di, self.vel_moy.dj)
            else:
                print("WARNING : file average_scal.out is missing.\n")
            
            print("\nExtraction of parameters -> OK")
            # ---- -- vel_raw 
                # Get "vel_raw" files 
            lstzip = foldzip.namelist()
            lstraw = [s for s in lstzip if "vel_raw" in s]
            lstraw = np.sort(lstraw[1:]) # Sort names 
            dictFiles["vel_raw"] = len(lstraw) # Add entry to follow-up checking dictionnary
                # Extract data, field by field
            for fich in tqdm (lstraw,desc="Raw Velocities"):
                buff    = foldzip.read(fich).decode()
                sLi     = len((buff.split("\n")[0].split()))
                tmp     = buff.split()
                data    = np.reshape(np.asarray(tmp,dtype = float),(-1,sLi)) # Shaping data
                    # Create the vel() element containing the datas 
                elVelRaw = velocities.vel()
                    # Get data from extraction 
                elVelRaw.i     = data[:,0].copy()# copy the values -> avoid references troubles 
                #elVelRaw.i     = self.PIV_param.ninj[0] - elVelRaw.i   # correct i ax 
                elVelRaw.j     = data[:,1].copy()
                elVelRaw.di    = data[:,2].copy()
                elVelRaw.di    = -elVelRaw.di    # correct i ax  
                elVelRaw.dj    = data[:,3].copy()
                elVelRaw.r     = data[:,4].copy()
                elVelRaw.Ar    = data[:,7].copy() 
                elVelRaw.vi    = elVelRaw.di / self.PIV_param.dt 
                elVelRaw.vj    = elVelRaw.dj / self.PIV_param.dt
                    # Compute other quantities 
                elVelRaw.nij   = np.sqrt(elVelRaw.di**2 + elVelRaw.dj**2)
                elVelRaw.x     = (elVelRaw.j * self.img_ref.res) + self.img_ref.xyMin[0] 
                elVelRaw.y     = (elVelRaw.i * self.img_ref.res) + self.img_ref.xyMin[1] 
                elVelRaw.dx    = elVelRaw.dj * self.img_ref.res
                elVelRaw.dy    = -elVelRaw.di * self.img_ref.res
                elVelRaw.vx    = elVelRaw.dx / self.PIV_param.dt
                elVelRaw.vy    = elVelRaw.dy / self.PIV_param.dt
                elVelRaw.n     = np.sqrt(elVelRaw.vx**2 + elVelRaw.vy**2)
                elVelRaw.theta = np.arctan2(elVelRaw.vy,elVelRaw.vx)
                    # Add the element to the list of velocities 
                self.vel_raw.append(elVelRaw)
            
            # ---- -- vel_filter 
                # Get "vel_filter" files 
            lstfilt = [s for s in lstzip if "vel_filter" in s]
            lstfilt = np.sort(lstfilt[1:]) # Sort
            dictFiles["vel_filter"] = len(lstfilt) # Add entry to follow-up checking dictionnary
                # Extract data, field by field
            for fich in tqdm (lstfilt,desc="Filtered Velocities"):
                buff = foldzip.read(fich)
                tmp = buff.split()
                data = np.reshape(np.asarray(tmp,dtype = float),(-1,5)) # Shaping data
                    # Create the vel() element containing the datas 
                elVelFilt = velocities.vel()
                    # Get data from extraction 
                elVelFilt.x  = data[:,0].copy()
                elVelFilt.y  = data[:,1].copy()
                elVelFilt.vx = data[:,2].copy()
                elVelFilt.vy = data[:,3].copy()
                elVelFilt.r  = data[:,4].copy()
                    # Compute other quantities 
                elVelFilt.n     = np.sqrt(elVelFilt.vx**2 + elVelFilt.vy**2)
                elVelFilt.theta = np.arctan2(elVelFilt.vy,elVelFilt.vx)
                elVelFilt.dx     = elVelFilt.vx * self.PIV_param.dt
                elVelFilt.dy     = elVelFilt.vy * self.PIV_param.dt
                elVelFilt.di     = - elVelFilt.dy / self.img_ref.res
                elVelFilt.dj     = elVelFilt.dx / self.img_ref.res 
                elVelFilt.vi     = elVelFilt.di / self.PIV_param.dt
                elVelFilt.vj     = elVelFilt.dj / self.PIV_param.dt
                elVelFilt.i      = self.PIV_param.ninj[0] - ((elVelFilt.y - self.img_ref.xyMin[1]) / self.img_ref.res)
                elVelFilt.j      = (elVelFilt.x - self.img_ref.xyMin[0]) / self.img_ref.res
                elVelFilt.nij    = np.sqrt(elVelFilt.di**2 + elVelFilt.dj**2)
                    # Add element to list 
                self.vel_filtre.append(elVelFilt)

            # ---- Verification
            self.contains = dictFiles
        
        return 0
    
     def display_raw(self,wd_p,Fname="/scatter_vxvy_raw.pdf",en=True):
        """
         Scatter plot of raw velocities (all time step and all grid points)

         Parameters
         ----------
         wd_p : str
             path to save file (without / at the end).
         Fname : str , optional
             File name. The default is "/scatter_vxvy_raw.pdf".
         en : bool, optional
             Figure's title in english (If false -> French). The default is True.

         Returns
         -------
         int
             errno.

         """
         
        with PdfPages(wd_p + Fname) as pdf:  
            # Set cmap
            cmap        = matplotlib.cm.get_cmap("magma")
            vx,vy,r = [],[],[]
            for el in self.vel_raw:
                vx = np.concatenate((vx,el.vx))
                vy = np.concatenate((vy,el.vy)) 
                r  = np.concatenate((r,el.r))
                
            cond    = np.logical_and(np.isfinite(vx),np.isfinite(r))  
            cond    = np.logical_and(cond,r!=-99.)
            
            vx      = vx[cond]
            vy      = vy[cond]
            r       = r[cond]
                        
            
            fig     = plt.figure(figsize=(8.2, 8))
            # Add a gridspec with two rows and two columns and a ratio of 2 to 7 between
            # the size of the marginal axes and the main axes in both directions.
            # Also adjust the subplot parameters for a square plot.
            gs      = fig.add_gridspec(2, 2,  width_ratios=(7, 2), height_ratios=(2, 7),
                                  left=0.1, right=0.9, bottom=0.1, top=0.9,
                                  wspace=0.05, hspace=0.05)
            
            ax      = fig.add_subplot(gs[1, 0])
            ax_histx = fig.add_subplot(gs[0, 0], sharex=ax)
            ax_histy = fig.add_subplot(gs[1, 1], sharey=ax)
            
            # no labels
            ax_histx.tick_params(axis="x", labelbottom=False)
            ax_histy.tick_params(axis="y", labelleft=False)
        
            # the scatter plot:
            ax.scatter(vx,
                       vy,
                       alpha     = 0.35,
                       c         = r,
                       cmap      = "magma",
                       edgecolor = None,
                       linewidth = 0.1,
                       vmin      = 0,
                       vmax      = 1)     
            # Histograms 
            # Ensure one bin of the histogram represent one pixel 
            # -> useful to have an estimation of the displacement in pixels !
            hx      = ax_histx.hist(vx, 
                                    bins= self.PIV_param.SA["Sjp"]+self.PIV_param.SA["Sjm"],
                          linewidth = 0.5,density=True)
            hy      = ax_histy.hist(vy, 
                                    bins=self.PIV_param.SA["Sip"]+self.PIV_param.SA["Sim"], 
                          orientation='horizontal',
                          linewidth = 0.5,density=True)   
            
            # Add colors on histograms 
            for ind,xl in enumerate(hx[1][:-1]):
                xlm = hx[1][ind+1]
                # Boucle sur les vitesses 
                r_  = []
                for k,vv in enumerate(vx):
                    if vv >= xl and vv <= xlm:
                        r_.append(r[k])
                rm  = np.mean(r_) 
                # Apply color on hist 
                hx[2][ind]._facecolor = cmap(rm) 
                
            for ind,yl in enumerate(hy[1][:-1]):
                ylm = hy[1][ind+1]
                # Boucle sur les vitesses 
                r_  = []
                for k,vv in enumerate(vy):
                    if vv >= yl and vv <= ylm:
                        r_.append(r[k])
                rm  = np.mean(r_) 
                # Apply color on hist 
                hy[2][ind]._facecolor = cmap(rm) 
                
            # Arrange axes 
            ax.grid(which='minor', linestyle=':', linewidth=0.5)
            ax.minorticks_on()
            ax.grid(which='major', linestyle='-', linewidth=0.75)
            ax.tick_params(which='minor', bottom=True, left=True)
            clb = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin=0, vmax=1),cmap = "magma")
            if en:
                cc  = plt.colorbar(clb,label = "Correlation")
            else:
                cc  = plt.colorbar(clb,label = "Corrélation")
                
            # Compute data for title 
            IAm = np.round(self.PIV_param.IA * self.img_ref.res,3)
                
            
            if en:
                ax_histx.set_title("Data: $v_{raw}$"+" - IA: {} pix $\longleftrightarrow$ {} m - framestep: {}".format(self.PIV_param.IA,IAm,self.ind2))
            else:
                ax_histx.set_title("Données: $v_{brutes}$"+" - IA: {} pix $\longleftrightarrow$ {} m - framestep: {}".format(self.PIV_param.IA,IAm,self.ind2))
            ax.annotate("SA :" ,(420,480),xycoords = "figure points",fontsize = 10)
            ax.annotate("$Sim =" + str(self.PIV_param.SA["Sim"])+"pix$" ,(420,465),xycoords = "figure points",fontsize = 10)
            vym = (self.PIV_param.SA["Sim"] * self.img_ref.res)/self.PIV_param.dt
            ax.annotate(" $Sym =-" + str(np.round(vym,3)) +"m/s$" ,(500,465),xycoords = "figure points",fontsize = 10)
            
            ax.annotate("$Sip =" + str(self.PIV_param.SA["Sip"])+"pix$" ,(420,450),xycoords = "figure points",fontsize = 10)
            vyp = (self.PIV_param.SA["Sip"] * self.img_ref.res)/self.PIV_param.dt
            ax.annotate(" $Syp =" + str(np.round(vyp,3)) +"m/s$" ,(500,450),xycoords = "figure points",fontsize = 10)
            
            ax.annotate("$Sjm =" + str(self.PIV_param.SA["Sjm"])+"pix$" ,(420,435),xycoords = "figure points",fontsize = 10)
            vxm = (self.PIV_param.SA["Sjm"] * self.img_ref.res)/self.PIV_param.dt
            ax.annotate(" $Sxm =-" + str(np.round(vxm,3)) +"m/s$" ,(500,435),xycoords = "figure points",fontsize = 10)
            
            ax.annotate("$Sjp =" + str(self.PIV_param.SA["Sjp"])+"pix$",(420,420),xycoords = "figure points",fontsize = 10)
            vxp = (self.PIV_param.SA["Sjp"] * self.img_ref.res)/self.PIV_param.dt
            ax.annotate(" $Sxp =" + str(np.round(vxp,3)) +"m/s$" ,(500,420),xycoords = "figure points",fontsize = 10)
             
            ax.set_xlabel("$v_x [m/s]$")
            ax.set_ylabel("$v_y [m/s]$")
            
            pdf.savefig()
            plt.close()
            
        return 0
            
     def display_filter(self,wd_p,Fname="/scatter_vxvy_filter.pdf",en=True):
        """
         Scatter plot of raw velocities (all time step and all grid points)

         Parameters
         ----------
         wd_p : str
             path to save file (without / at the end).
         Fname : str , optional
             File name. The default is "/scatter_vxvy_raw.pdf".
         en : bool, optional
             Figure's title in english (If false -> French). The default is True.

         Returns
         -------
         int
             errno.

         """
         
        with PdfPages(wd_p + Fname) as pdf:  
            # Set cmap
            cmap        = matplotlib.cm.get_cmap("magma")
            vx,vy,r = [],[],[]
            for el in self.vel_filtre:
                vx = np.concatenate((vx,el.vx))
                vy = np.concatenate((vy,el.vy)) 
                r  = np.concatenate((r,el.r))
                
            cond    = np.logical_and(np.isfinite(vx),np.isfinite(r))  
            cond    = np.logical_and(cond,r!=-99.)
            
            vx      = vx[cond]
            vy      = vy[cond]
            r       = r[cond]
                        
            
            fig     = plt.figure(figsize=(8.2, 8))
            # Add a gridspec with two rows and two columns and a ratio of 2 to 7 between
            # the size of the marginal axes and the main axes in both directions.
            # Also adjust the subplot parameters for a square plot.
            gs      = fig.add_gridspec(2, 2,  width_ratios=(7, 2), height_ratios=(2, 7),
                                  left=0.1, right=0.9, bottom=0.1, top=0.9,
                                  wspace=0.05, hspace=0.05)
            
            ax      = fig.add_subplot(gs[1, 0])
            ax_histx = fig.add_subplot(gs[0, 0], sharex=ax)
            ax_histy = fig.add_subplot(gs[1, 1], sharey=ax)
            
            # no labels
            ax_histx.tick_params(axis="x", labelbottom=False)
            ax_histy.tick_params(axis="y", labelleft=False)
        
            # the scatter plot:
            ax.scatter(vx,
                       vy,
                       alpha     = 0.35,
                       c         = r,
                       cmap      = "magma",
                       edgecolor = None,
                       linewidth = 0.1,
                       vmin      = 0,
                       vmax      = 1)     
            # Histograms 
            # Ensure one bin of the histogram represent one pixel 
            # -> useful to have an estimation of the displacement in pixels !
            hx      = ax_histx.hist(vx, 
                                    bins= self.PIV_param.SA["Sjp"]+self.PIV_param.SA["Sjm"],
                          linewidth = 0.5,density=True)
            hy      = ax_histy.hist(vy, 
                                    bins=self.PIV_param.SA["Sip"]+self.PIV_param.SA["Sim"], 
                          orientation='horizontal',
                          linewidth = 0.5,density=True)   
            
            # Add colors on histograms 
            for ind,xl in enumerate(hx[1][:-1]):
                xlm = hx[1][ind+1]
                # Boucle sur les vitesses 
                r_  = []
                for k,vv in enumerate(vx):
                    if vv >= xl and vv <= xlm:
                        r_.append(r[k])
                rm  = np.mean(r_) 
                # Apply color on hist 
                hx[2][ind]._facecolor = cmap(rm) 
                
            for ind,yl in enumerate(hy[1][:-1]):
                ylm = hy[1][ind+1]
                # Boucle sur les vitesses 
                r_  = []
                for k,vv in enumerate(vy):
                    if vv >= yl and vv <= ylm:
                        r_.append(r[k])
                rm  = np.mean(r_) 
                # Apply color on hist 
                hy[2][ind]._facecolor = cmap(rm) 
                
            # Arrange axes 
            ax.grid(which='minor', linestyle=':', linewidth=0.5)
            ax.minorticks_on()
            ax.grid(which='major', linestyle='-', linewidth=0.75)
            ax.tick_params(which='minor', bottom=True, left=True)
            clb = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin=0, vmax=1),cmap = "magma")
            if en:
                cc  = plt.colorbar(clb,label = "Correlation")
            else:
                cc  = plt.colorbar(clb,label = "Corrélation")
                
            # # Compute data for title 
            # IAm = np.round(self.PIV_param.IA * self.img_ref.res,3)
                
            
            # if en:
            #     ax_histx.set_title("Data: $v_{raw}$"+" - IA: {} pix $\longleftrightarrow$ {} m - framestep: {}".format(self.PIV_param.IA,IAm,self.ind2))
            # else:
            #     ax_histx.set_title("Données: $v_{brutes}$"+" - IA: {} pix $\longleftrightarrow$ {} m - framestep: {}".format(self.PIV_param.IA,IAm,self.ind2))
            # ax.annotate("SA :" ,(420,480),xycoords = "figure points",fontsize = 10)
            # ax.annotate("$Sim =" + str(self.PIV_param.SA["Sim"])+"pix$" ,(420,465),xycoords = "figure points",fontsize = 10)
            # vym = (self.PIV_param.SA["Sim"] * self.img_ref.res)/self.PIV_param.dt
            # ax.annotate(" $Sym =-" + str(np.round(vym,3)) +"m/s$" ,(500,465),xycoords = "figure points",fontsize = 10)
            
            # ax.annotate("$Sip =" + str(self.PIV_param.SA["Sip"])+"pix$" ,(420,450),xycoords = "figure points",fontsize = 10)
            # vyp = (self.PIV_param.SA["Sip"] * self.img_ref.res)/self.PIV_param.dt
            # ax.annotate(" $Syp =" + str(np.round(vyp,3)) +"m/s$" ,(500,450),xycoords = "figure points",fontsize = 10)
            
            # ax.annotate("$Sjm =" + str(self.PIV_param.SA["Sjm"])+"pix$" ,(420,435),xycoords = "figure points",fontsize = 10)
            # vxm = (self.PIV_param.SA["Sjm"] * self.img_ref.res)/self.PIV_param.dt
            # ax.annotate(" $Sxm =-" + str(np.round(vxm,3)) +"m/s$" ,(500,435),xycoords = "figure points",fontsize = 10)
            
            # ax.annotate("$Sjp =" + str(self.PIV_param.SA["Sjp"])+"pix$",(420,420),xycoords = "figure points",fontsize = 10)
            # vxp = (self.PIV_param.SA["Sjp"] * self.img_ref.res)/self.PIV_param.dt
            # ax.annotate(" $Sxp =" + str(np.round(vxp,3)) +"m/s$" ,(500,420),xycoords = "figure points",fontsize = 10)
             
            # ax.set_xlabel("$v_x [m/s]$")
            # ax.set_ylabel("$v_y [m/s]$")
            
            pdf.savefig()
            plt.close()
            
        return 0
    
     def computeAverage(self,boolMean = True):
         
         # Create element 
        mean = self.vel_raw[0].copy()
        # Run over grid points 
        for ind,el in enumerate(mean.vx):
            vx,vy,dx,dy,n,r,crms,di,dj = np.empty(0),np.empty(0),np.empty(0),np.empty(0),np.empty(0),np.empty(0),np.empty(0),np.empty(0),np.empty(0)
            # Extract results at point
            for filtre in self.vel_filtre:
                vx = np.append(vx,filtre.vx[ind])
                vy = np.append(vy,filtre.vy[ind])
                dx = np.append(dx,filtre.dx[ind])
                dy = np.append(dy,filtre.dy[ind])
                di = np.append(di,filtre.di[ind])
                dj = np.append(dj,filtre.dj[ind])
                n  = np.append(n,filtre.n[ind])
                r  = np.append(r,filtre.r[ind])
                #crms = np.append(crms,filtre.crms[ind])
            # Remove filtered points
            cond   = np.logical_and(vx==0,vy ==0)
            vx[cond] = np.nan
            # Remove nan 
            di = di[np.isfinite(vx)]
            dj = dj[np.isfinite(vx)]
            vy = vy[np.isfinite(vx)]
            dx = dx[np.isfinite(vx)]
            dy = dy[np.isfinite(vx)]
            n  = n[np.isfinite(vx)]
            r  = r[np.isfinite(vx)]
            vx = vx[np.isfinite(vx)]
            #crms  = crms[np.isfinite(crms)]
            # Compute mean 
            if boolMean:
                    mean.vx[ind] = np.mean(vx)
                    mean.vy[ind] = np.mean(vy)
                    mean.dx[ind] = np.mean(dx)
                    mean.dy[ind] = np.mean(dy)
                    mean.n[ind]  = np.mean(n)
                    mean.r[ind]  = np.mean(r)
                    mean.di[ind] = np.mean(di)
                    mean.dj[ind] = np.mean(dj)
            else:
                    mean.vx[ind] = np.median(vx)
                    mean.vy[ind] = np.median(vy)
                    mean.dx[ind] = np.median(dx)
                    mean.dy[ind] = np.median(dy)
                    mean.n[ind]  = np.median(n)
                    mean.di[ind] = np.median(di)
                    mean.dj[ind] = np.median(dj)

            
        self.vel_moy = mean   
        
     def writeAverageOut(self,wd):
         """
         Write file average_vel.out in specified folder (wd). 
         This function is mainly used to compute discharge out of F-LSPIV as average_vel.out is used to obtain surface velocities on the transect

         Parameters
         ----------
         wd : str
             Path to the folder were average_vel.out will be written.

         Returns
         -------
         int
             errno.

         """
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if not isinstance(self.vel_moy,list):
             f      = open(wd + "/average_vel.out","w")
             for ind,el in enumerate(self.vel_moy.x):
                 x  = np.round(self.vel_moy.x[ind],4)
                 y  = np.round(self.vel_moy.y[ind],4)
                 vx = np.round(self.vel_moy.vx[ind],4)
                 vy = np.round(self.vel_moy.vy[ind],4)
                 li = "{} {} {} {}\n".format(x,y,vx,vy)
                 li.replace("nan","0.0")
                 f.write(li.replace("nan","0.0"))
             f.close()
            
         return 0
     
     def writeFilterPiv(self,wd):
         
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if len(self.vel_filtre) > 0:
             for ind,el in enumerate(self.vel_filtre):
                 f      = open(wd + "/filter_piv%04d.dat" % (ind+1),"w")
                 for x,y,vx,vy,r in zip(el.x,el.y,el.vx,el.vy,el.r):
                     x  = np.round(x,4)
                     y  = np.round(y,4)
                     vx = np.round(vx,4)
                     vy = np.round(vy,4)
                     li = "{} {} {} {}\n".format(x,y,vx,vy)
                     li.replace("nan","0.0")
                     f.write(li.replace("nan","0.0"))
                 f.close()
         else: 
             print("ERROR: no filter velocities")
             return 1
         
         return 0
     
     def writeRawPiv(self,wd):
         
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if len(self.vel_raw) > 0:
             for ind,el in enumerate(self.vel_raw):
                 f      = open(wd + "/piv%04d.dat" % (ind+1),"w")
                 for x,y,vx,vy,r in zip(el.x,el.y,el.vx,el.vy,el.r):
                     x  = np.round(x,4)
                     y  = np.round(y,4)
                     vx = np.round(vx,4)
                     vy = np.round(vy,4)
                     li = "{} {} {} {}\n".format(x,y,vx,vy)
                     li.replace("nan","0.0")
                     f.write(li.replace("nan","0.0"))
                 f.close()
         else: 
             print("ERROR: no filter velocities")
             return 1
         
         return 0
    
     def genRawPp(self):
            """
            Arrange data "per point". 
        
            Parameters
            ----------
        
            Returns
            -------
                - erno [int]: 
                    Error number 
        
            """
            if(len(self.vel_raw) > 1):
                # Create list and get info 
                self.vel_raw_pp      = []
                nbPt            = len(self.vel_raw[0].di) 
                # Initialize 
                for j in range(nbPt):
                    self.vel_raw_pp.append(velocities.vel(0)) 
                # Run over results
                for k,el in enumerate(self.vel_raw):
                     for j in range(nbPt):
                         self.vel_raw_pp[j].di = np.append(self.vel_raw_pp[j].di, el.di[j])
                         self.vel_raw_pp[j].dj = np.append(self.vel_raw_pp[j].dj, el.dj[j])
                         self.vel_raw_pp[j].vx = np.append(self.vel_raw_pp[j].vx, el.vx[j])
                         self.vel_raw_pp[j].vy = np.append(self.vel_raw_pp[j].vy, el.vy[j])
                         self.vel_raw_pp[j].n  = np.append(self.vel_raw_pp[j].n, el.di[j])
                         self.vel_raw_pp[j].i  = np.append(self.vel_raw_pp[j].i, el.i[j])
                         self.vel_raw_pp[j].j  = np.append(self.vel_raw_pp[j].j, el.j[j])
                         self.vel_raw_pp[j].r  = np.append(self.vel_raw_pp[j].r, el.r[j])
                         self.vel_raw_pp[j].x  = np.append(self.vel_raw_pp[j].x, el.x[j])
                         self.vel_raw_pp[j].y  = np.append(self.vel_raw_pp[j].y, el.y[j])
                
            else:
                print("No raw velocities, can't generate per point data...")
                return 1
                
            return 0
        
     def genFilterPp(self):
            """
            Arrange data "per point". 
        
            Parameters
            ----------
        
            Returns
            -------
                - erno [int]: 
                    Error number 
        
            """
            if(len(self.vel_filtre) > 1):
                # Create list and get info 
                self.vel_filtre_pp      = []
                nbPt            = len(self.vel_filtre[0].di) 
                for j in range(nbPt):
                    self.vel_filtre_pp.append(velocities.vel(0)) 
                # Run over results
                for k,el in enumerate(self.vel_filtre):
                     for j in range(nbPt):
                         self.vel_filtre_pp[j].di = np.append(self.vel_filtre_pp[j].di, el.di[j])
                         self.vel_filtre_pp[j].dj = np.append(self.vel_filtre_pp[j].dj, el.dj[j])
                         self.vel_filtre_pp[j].vx = np.append(self.vel_filtre_pp[j].vx, el.vx[j])
                         self.vel_filtre_pp[j].vy = np.append(self.vel_filtre_pp[j].vy, el.vy[j])
                         self.vel_filtre_pp[j].n  = np.append(self.vel_filtre_pp[j].n, el.di[j])
                         self.vel_filtre_pp[j].i  = np.append(self.vel_filtre_pp[j].i, el.i[j])
                         self.vel_filtre_pp[j].j  = np.append(self.vel_filtre_pp[j].j, el.j[j])
                         self.vel_filtre_pp[j].r  = np.append(self.vel_filtre_pp[j].r, el.r[j])
                         self.vel_filtre_pp[j].x  = np.append(self.vel_filtre_pp[j].x, el.x[j])
                         self.vel_filtre_pp[j].y  = np.append(self.vel_filtre_pp[j].y, el.y[j])
                
                         
            else:
                print("No filtered velocities, can't generate per point data...")
                return 1
            
            return 0
    
     def getGrid(self):
         # Return grid 
         if len(self.grid) > 1:
             return self.grid
         else:
             # Get grid from vel
             if len(self.vel_raw) > 1:
                 i,j = self.vel_raw[0].i,self.vel_raw[0].j
                 grid  = np.array([i,j]).T    
                 return grid 
             else:
                 return None
             
     def load_Q(self,wd):
         
         # Get results from file Discharge.dat
         try :
             f       = open(wd + "/Discharge.dat","r")
             buff    = f.read().split("\n")
             f.close() 
         except Exception as e:
            print("\nERROR: Problem opening Discharge.dat \n Exception: " + str(e))
            return 1
         
         # Extract discharge data
         Q          = {}
         l          = buff[0].split()
         Q["Total"] = float(l[1])
         Q["Area"]  = float(l[2])
         Q["Measured"]  = float(l[4])
         self.Q     = Q
         
         # Extract transect velocities
         lst        = buff[1:-1]
         transect   = bathy()
         for el in lst:
             tmp    = el.split()
             transect.X.append(float(tmp[2]))
             transect.Y.append(float(tmp[3]))
             transect.Z.append(float(tmp[4]))
             transect.vx.append(float(tmp[5]))
             transect.vy.append(float(tmp[6]))
             transect.vn.append(float(tmp[7]))
             transect.alpha.append(float(tmp[9]))
             
         self.transect.append(transect)
         
         return 0
         
     def import_img_src(self,path = ""):
        """
        Import source images to the lspiv project. path identifies the folder containing the images. 
        If no path is given then a GUI will be open to find it
    
        Parameters
        ----------
            - path : [str], optional
                path to the folder containing the source images. If no path is given then a GUI will be open 
                The default is "".
    
        Returns
        -------
            - errno [int]
                - Error number 
                    - 0 - No errors 
                    - 1 - Wrong format 

        """
        if len(self.img_src) != 0:
            print("\nWARNING: source images already exist. Overwrite ? (y|n)")
            rep = input().lower()
            if rep == "y":
                self.img_src = []
            elif rep == "n":
                print("\nAbort import")
                return 0
         # ---- Initialisation 
        isExist = os.path.exists(path) # Check if file exists 
        
        if (path == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            path = askdirectory(title='Select Image Folder') 
            
        # Checkings
        lstfiles = glob.glob(path + "/*.png")
        if(len(lstfiles)==0):
            print("\nERROR: No images located in the scpecified folder. Image format expected: image*.png !")
            return 1 
        
        # ---- Read images 
        lstfiles = np.sort(lstfiles) # sort names
        try:
            for name in lstfiles:
                im      = Image.open(name)
                imArray = np.asarray(im)
                self.img_src.append(imArray)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1
            
        return 0
    
     def import_img_transf(self,path = ""):
        """
        Import transformed images to the lspiv project. path identifies the folder containing the images. 
        If no path is given then a GUI will be open to find it
    
        Parameters
        ----------
            - path : [str], optional
                path to the folder containing the source images. If no path is given then a GUI will be open 
                The default is "".
    
        Returns
        -------
            - errno [int]
                - Error number 
                    - 0 - No errors 
                    - 1 - Wrong format 

        """
        # Checkings
        if len(self.img_transf) != 0:
            print("\nWARNING: source images already exist. Overwrite ? (y|n)")
            rep = input().lower()
            if rep == "y":
                self.img_transf = []
            elif rep == "n":
                print("\nAbort import")
                return 0
        
         # ---- Initialisation 
        isExist = os.path.exists(path) # Check if file exists 
        
        if (path == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            path = askdirectory(title='Select Image Folder') 
            
        # Checkings
        lstfiles = glob.glob(path + "/image*.png")
        if(len(lstfiles)==0):
            print("\nERROR: No images located in the scpecified folder. Image format expected: image*.png !")
            return 1 
        
        # ---- Read images 
        lstfiles = np.sort(lstfiles) # sort names
        try:
            for name in lstfiles:
                im      = Image.open(name)
                imArray = np.asarray(im)
                self.img_transf.append(imArray)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1
            
        return 0
     def import_mask(self,filename = ""):
        """
        Import a mask (binary image) of the river flow

        Parameters
        ----------
            filename : [str], optional
                Path to the file 'mask.png'. If "" then a GUI will be opened to select file
                The default is "".

        Returns
        -------
            - errno [int]
                Error number. Possible values : 
                    - 0 - no errors 
                    - 1 - wrong format contains file coeff.datfilename : TYPE, optional
        """
        # ---- Initialisation 
        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename(title = "Select mask.png", filetypes = (("mask","mask.png"),("all files","*.*"))) # Get file path
        
        # ---- Read 
        try: 
            self.mask = Image.open(filename)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1 
        
     def add_vel_pp(self,raw = True, filter = True):
         """
         Add list of velocity class "per point". vel_XX_pp is a list of size nbGRP containing sublists of size nbImgs. 
         Very usefull for temporal studies of results at each point

         Parameters
         ----------
             - raw : bool, optional
                 Set if the raw velocities have to be transformed to "per point". The default is True.
             - filter : bool, optional
                 Set if the raw velocities have to be transformed to "per point". The default is True.

         Returns
         -------
             - errno : int
                 Error number : - 0 - no errors 
                                - 1 - no velocities found in lspivProject file

         """
         
         # Work on raw velocities 
         if raw:
             # Checkings 
             if len(self.vel_raw) != 0:
                 print("ERROR: no velocities found in vel_raw !")
                 return 1
             # Create list and get info 
             vel_raw_pp = []
             nbIt       = len(self.vel_raw)         # Number of intervals (or frame/step)
             nbPt       = len(self.vel_raw[0].di)   # Number of points on grid
             # Initialization of elements 
             for k in range(nbPt):
                 newEl  = velocities.vel(size = nbIt)
                 vel_raw_pp.append(newEl)
             # Fill in elements of list  
             for k,el in enumerate(self.vel_raw):
                  for j in range(nbPt):
                      vel_raw_pp[j].di[k]   = el.di[j]
                      vel_raw_pp[j].dj[k]   = el.dj[j]
                      vel_raw_pp[j].dx[k]   = el.dx[j]
                      vel_raw_pp[j].dy[k]   = el.dy[j]
                      vel_raw_pp[j].i[k]    = el.i[j]
                      vel_raw_pp[j].j[k]    = el.j[j]
                      vel_raw_pp[j].n[k]    = el.n[j]
                      vel_raw_pp[j].nij[k]  = el.nij[j]
                      vel_raw_pp[j].r[k]    = el.r[j]
                      vel_raw_pp[j].vi[k]   = el.vi[j]
                      vel_raw_pp[j].vj[k]   = el.vj[j]
                      vel_raw_pp[j].vx[k]   = el.vx[j]
                      vel_raw_pp[j].vy[k]   = el.vy[j]
                      vel_raw_pp[j].theta[k] = el.theta[j]
                      vel_raw_pp[j].x[k]    = el.x[j]
                      vel_raw_pp[j].y[k]    = el.y[j]
             # Add list to object lspivProject 
             self.vel_raw_pp = vel_raw_pp.copy()
             
             # Work on filtered velocities 
             if filter:
                 # Checkings 
                 if len(self.vel_filter) != 0:
                     print("ERROR: no velocities found in vel_raw !")
                     return 1
                 # Create list and get info 
                 vel_filtre_pp = []
                 nbIt       = len(self.vel_raw)         # Number of intervals (or frame/step)
                 nbPt       = len(self.vel_raw[0].di)   # Number of points on grid
                 # Initialization of elements 
                 for k in range(nbPt):
                     newEl  = velocities.vel(size = nbIt)
                     vel_raw_pp.append(newEl)
                 # Fill in elements of list  
                 for k,el in enumerate(self.vel_raw):
                      for j in range(nbPt):
                          vel_filtre_pp[j].di[k]    = el.di[j]
                          vel_filtre_pp[j].dj[k]    = el.dj[j]
                          vel_filtre_pp[j].dx[k]    = el.dx[j]
                          vel_filtre_pp[j].dy[k]    = el.dy[j]
                          vel_filtre_pp[j].i[k]     = el.i[j]
                          vel_filtre_pp[j].j[k]     = el.j[j]
                          vel_filtre_pp[j].n[k]     = el.n[j]
                          vel_filtre_pp[j].nij[k]   = el.nij[j]
                          vel_filtre_pp[j].r[k]     = el.r[j]
                          vel_filtre_pp[j].vi[k]    = el.vi[j]
                          vel_filtre_pp[j].vj[k]    = el.vj[j]
                          vel_filtre_pp[j].vx[k]    = el.vx[j]
                          vel_filtre_pp[j].vy[k]    = el.vy[j]
                          vel_filtre_pp[j].theta[k] = el.theta[j]
                          vel_filtre_pp[j].x[k]     = el.x[j]
                          vel_filtre_pp[j].y[k]     = el.y[j]
                 # Add list to object lspivProject 
                 self.vel_filtre_pp = vel_filtre_pp.copy()
                 
             return 0 
     
            
     def temporalFiltering(self,dest,k=3,tCoV = 0.4, tR = 0.25,CoV = True, dev = True):
         
         
         if self.transect.a != 0 or self.transect.b !=0:
             a,b = self.transect.a, self.transect.b
             transect = True
             
         if dest == "raw":
             # Create filter velocities
             self.vel_filtre = []
             for el in self.vel_raw:
                 self.vel_filtre.append(el.copy())

         else:

             self.genFilterPp()
             varA_,varA_2,fCoV_ = [],[],[]
             # Run over grid points 
             for ind,el in tqdm(enumerate(self.vel_filtre_pp),desc="Temporal filtering..."):
                 di     = el.di
                 dj     = el.dj
                 # Force non measured points (where di==0 and dj==0) to nan before mean computation
                 cond   = np.logical_or(di==0,dj ==0)
                 di[cond] = np.nan
                 dj[cond] = np.nan
                
                    
                 if dev: 
                   # ## FILTER 2 - DEVIATION FROM MEAN VELOCITY
                   # ##################################
                        # Evaluate mean and std at each point for di/dj 
                        meanI  = np.nanmean(di)
                        stdI   = np.nanstd(di)
                        meanJ  = np.nanmean(dj)
                        stdJ   = np.nanstd(dj)
                        # Temporal filter (k*sigma tolerance)
                        mnI,mxI= meanI-(k*stdI),meanI+(k*stdI)
                        mnJ,mxJ= meanJ-(k*stdJ),meanJ+(k*stdJ)
                        # Apply filter 
                        fI     = np.logical_or(di>mxI,di<mnI)
                        fJ     = np.logical_or(dj>mxJ,dj<mnJ)
                      
                        stpI   = np.where(fI==True)
                        stpJ   = np.where(fJ==True)
       
                        # Apply on pp velocities 
                        el.di[stpI] = np.nan
                        el.dj[stpI] = np.nan
                        el.r[stpI]  = np.nan
                        el.vx[stpI] = np.nan
                        el.vy[stpI] = np.nan
                        
                        el.di[stpJ] = np.nan
                        el.dj[stpJ] = np.nan
                        el.r[stpJ]  = np.nan
                        el.vx[stpJ] = np.nan
                        el.vy[stpJ] = np.nan
                        
                        for stp in stpI[0]:
                           self.vel_filtre[stp].vx[ind] = np.nan
                           self.vel_filtre[stp].vy[ind] = np.nan
                           self.vel_filtre[stp].dx[ind] = np.nan
                           self.vel_filtre[stp].dy[ind] = np.nan
                           self.vel_filtre[stp].di[ind] = np.nan
                           self.vel_filtre[stp].dj[ind] = np.nan
                           self.vel_filtre[stp].r[ind] = np.nan
                           
                        
                        for stp in stpJ[0]:
                           self.vel_filtre[stp].vx[ind] = np.nan
                           self.vel_filtre[stp].vy[ind] = np.nan
                           self.vel_filtre[stp].dx[ind] = np.nan
                           self.vel_filtre[stp].dy[ind] = np.nan
                           self.vel_filtre[stp].di[ind] = np.nan
                           self.vel_filtre[stp].dj[ind] = np.nan
                           self.vel_filtre[stp].r[ind] = np.nan
                          
                 # ## FILTER 2 - COEFF. OF VARIATION
                 # ##################################
                 vx     = el.vx
                 vy     = el.vy
                 # Force non measured points (where di==0 and dj==0) to nan before mean computation
                 cond   = np.logical_or(vx==0,vy ==0)
                 vx[cond] = np.nan
                 vy[cond] = np.nan
                 
                 if transect:
                     # Project velocity on vector normal to transect
                     vp     = -b*vx+a*vy
                     # Compute coefficient of variation on projected velocity 
                     varVP  = np.nanstd(vp)/np.nanmean(vp)
                 
                 # Compute coefficient of variation on angle 
                 aV     = np.arctan2(vy,vx)
                 # Store in list to compute mean and std
                 meanA  = scipy.stats.circmean(aV,high = np.pi,low=-np.pi,nan_policy="omit")
                 R      = scipy.stats.circvar(aV,high = np.pi,low=-np.pi,nan_policy="omit")
                 
                 # Center angle around mean
                 # aVC = (aV-meanA)
                 # aVCC = []
                 # # Account for angle out of [-pi,pi]
                 # for i in aVC:
                 #     if (i < np.pi and i> -np.pi):
                 #         aVCC.append(i)
                 #     elif i >= np.pi:
                 #         aVCC.append((np.pi*2)-i)
                 #     else:
                 #         aVCC.append(i%(2*np.pi))
                
                 # stdA    = np.nanstd(aVCC)
                                               
                # Filter out all velocities at the point based on CoV
                 if CoV and (varVP>tCoV or R>0.25):
                    
                    el.di[:] = np.nan
                    el.dj[:] = np.nan
                    el.r[:]  = np.nan
                    el.vx[:] = np.nan
                    el.vy[:] = np.nan
                   
                    el.di[:] = np.nan
                    el.dj[:] = np.nan
                    el.r[:]  = np.nan
                    el.vx[:] = np.nan
                    el.vy[:] = np.nan
                      
                    for stp in np.arange(len(di)):
                        self.vel_filtre[stp].vx[ind] = np.nan
                        self.vel_filtre[stp].vy[ind] = np.nan
                        self.vel_filtre[stp].dx[ind] = np.nan
                        self.vel_filtre[stp].dy[ind] = np.nan
                        self.vel_filtre[stp].di[ind] = np.nan
                        self.vel_filtre[stp].dj[ind] = np.nan
                        self.vel_filtre[stp].r[ind] = np.nan

                          
                      
                    
         return 0
                   
     def applyOffsetGrid(self,x0,y0):
         
         if len(self.vel_raw) > 0:
             for el in self.vel_raw:
                 el.x = el.x + x0
                 el.y = el.y + y0
                 
         if len(self.vel_filtre) > 0:
             for el in self.vel_filtre:
                 el.x = el.x + x0
                 el.y = el.y + y0
                 
         if isinstance(self.vel_moy,velocities.vel) > 0:
            self.vel_moy.x = self.vel_moy.x + x0
            self.vel_moy.y = self.vel_moy.y + y0
                 
                
class bathy():
    """ 
    Bathymetry info
    
    """
    def __init__(self): 
        self.X       = []  
        self.Y       = []
        self.Z       = []
        self.vx      = []
        self.vy      = []
        self.vn      = []
        self.xp      = []
        self.alpha   = [] 
        self.bExtrap = []
        self.a       = 0
        self.b       = 0
        self.x0      = 0
        self.y0      = 0
             
             
         
